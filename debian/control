Source: octave-bim
Section: math
Priority: optional
Maintainer: Debian Octave Group <team+pkg-octave-team@tracker.debian.org>
Uploaders: Thomas Weber <tweber@debian.org>,
           Rafael Laboissière <rafael@debian.org>,
           Sébastien Villemot <sebastien@debian.org>,
           Mike Miller <mtmiller@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-octave,
               libhdf5-openmpi-dev,
               octave-msh
Standards-Version: 4.6.1
Homepage: https://octave.sourceforge.io/bim/
Vcs-Git: https://salsa.debian.org/pkg-octave-team/octave-bim.git
Vcs-Browser: https://salsa.debian.org/pkg-octave-team/octave-bim
Testsuite: autopkgtest-pkg-octave
Rules-Requires-Root: no

Package: octave-bim
Architecture: all
Depends: ${misc:Depends}, ${octave:Depends}
Description: PDE solver using a finite element/volume approach in Octave
 This package contains scripts for solving Diffusion Advection
 Reaction (DAR) Partial Differential Equations based on the Finite
 Volume Scharfetter-Gummel (FVSG) method a.k.a Box Integration Method
 (BIM) in Octave, a scientific computation software.
 .
 This Octave add-on package is part of the Octave-Forge project.
